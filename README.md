# webApp

## Getting start

Prerequis:

- installer pecl:

```bash
sudo apt-get install \
php \
php-pear \
php8.1-dev \
libcurl3-openssl-dev
```

- puis installer l'extension mongodb:

```bash
sudo pecl upgrade mongodb
```

- modifier les php.ini:

```bash
cli: vim /etc/php/8.1/cli/php.ini
et ajouter extension=mongodb.so
```

```bash
sur apache2: vim /etc/php/8.1/apache2/php.ini
et ajouter extension=mongodb.so
```

- Enfin installer les lib:

```bash
sudo apt install composer
sudo composer install
```