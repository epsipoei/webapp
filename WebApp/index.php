<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Management System</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>User Management System</h1>
        <!-- Liens pour ajouter un utilisateur, un produit et une catégorie -->
        <a href="add_user.php" class="btn btn-primary">Add User</a>
        <a href="add_product.php" class="btn btn-primary">Add Product</a>
        <a href="add_category.php" class="btn btn-primary">Add Category</a>
        <!-- Liste des utilisateurs -->
        <h2>Users</h2>
        <ul>
            <?php
            require 'assets/includes/config.php';
            require 'assets/classes/User.php';

            $user = new User($usersCollection);
            $users = $user->readUser();

            foreach ($users as $userData) {
                echo "<li>" . htmlspecialchars($userData['firstName']) . " " . htmlspecialchars($userData['lastName']) . "</li>";
            }
            ?>
        </ul>
         <!-- Liste des produits -->
         <h2>Products</h2>
        <ul>
            <?php
            require 'assets/includes/config.php';
            require 'assets/classes/Product.php';

            $product = new Product($productsCollection);
            $products = $product->readProduct();

            foreach ($products as $productData) {
                echo "<li>" . htmlspecialchars($productData['productName']) . "</li>";
            }
            ?>
        </ul>
         <!-- Liste des categories -->
         <h2>Categories</h2>
        <ul>
            <?php
            require 'assets/includes/config.php';
            require 'assets/classes/Category.php';

            $category = new Category($categoriesCollection);
            $categories = $category->readCategory();

            foreach ($categories as $categoryData) {
                echo "<li>" . htmlspecialchars($categoryData['categoryName']) . "</li>";
            }
            ?>
        </ul>
    </div>
</body>
</html>
