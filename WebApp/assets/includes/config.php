<?php
require 'vendor/autoload.php';

use MongoDB\Client as MongoDBClient;
use MongoDB\Driver\Exception\Exception as MongoDBException;

try {
    $mongoDBClient = new MongoDBClient("<path mongodb for vscode>");

    // Sélectionner la base de données et les collections
    $database = $mongoDBClient->selectDatabase('WebApp');
    $usersCollection = $database->users;
    $productsCollection = $database->products;
    $categoriesCollection = $database->categories;
} catch (MongoDBException $e) {
    // Gérer les exceptions MongoDB
    echo "Erreur lors de la connexion à MongoDB : " . $e->getMessage();
    exit;
}
?>
