<?php
class Category {
    private $collection;

    public function __construct($collection) {
        $this->collection = $collection;
    }

    public function createCategory($categoryData) {
        try {
            $this->collection->insertOne($categoryData);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la création de la catégorie : " . $e->getMessage();
            exit;
        }
    }

    public function readCategory($filter = []) {
        try {
            return $this->collection->find($filter);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la lecture des catégories : " . $e->getMessage();
            exit;
        }
    }

    public function updateCategory($filter, $updateData) {
        try {
            $this->collection->updateOne($filter, ['$set' => $updateData]);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la mise à jour de la catégorie : " . $e->getMessage();
            exit;
        }
    }

    public function deleteCategory($filter) {
        try {
            $this->collection->deleteOne($filter);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la suppression de la catégorie : " . $e->getMessage();
            exit;
        }
    }
}
?>