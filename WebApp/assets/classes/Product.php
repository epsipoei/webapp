<?php
class Product {
    private $collection;

    public function __construct($collection) {
        $this->collection = $collection;
    }

    public function createProduct($productData) {
        try {
            $this->collection->insertOne($productData);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la création du produit : " . $e->getMessage();
            exit;
        }
    }

    public function readProduct($filter = []) {
        try {
            return $this->collection->find($filter);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la lecture des produits : " . $e->getMessage();
            exit;
        }
    }

    public function updateProduct($filter, $updateData) {
        try {
            $this->collection->updateOne($filter, ['$set' => $updateData]);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la mise à jour du produit : " . $e->getMessage();
            exit;
        }
    }

    public function deleteProduct($filter) {
        try {
            $this->collection->deleteOne($filter);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la suppression du produit : " . $e->getMessage();
            exit;
        }
    }
}
?>