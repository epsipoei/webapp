<?php
class User {
    private $collection;

    public function __construct($collection) {
        $this->collection = $collection;
    }

    public function createUser($userData) {
        try {
            $this->collection->insertOne($userData);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la création de l'utilisateur : " . $e->getMessage();
            exit;
        }
    }

    public function readUser($filter = []) {
        try {
            return $this->collection->find($filter);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la lecture des utilisateurs : " . $e->getMessage();
            exit;
        }
    }

    public function updateUser($filter, $updateData) {
        try {
            $this->collection->updateOne($filter, ['$set' => $updateData]);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la mise à jour de l'utilisateur : " . $e->getMessage();
            exit;
        }
    }

    public function deleteUser($filter) {
        try {
            $this->collection->deleteOne($filter);
        } catch (MongoDBException $e) {
            echo "Erreur lors de la suppression de l'utilisateur : " . $e->getMessage();
            exit;
        }
    }
}
?>
