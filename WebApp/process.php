<?php
require 'assets/includes/config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        // Validation des données d'entrée
        $firstName = isset($_POST['firstName']) ? htmlspecialchars($_POST['firstName']) : null;
        $lastName = isset($_POST['lastName']) ? htmlspecialchars($_POST['lastName']) : null;

        if ($firstName !== null && $lastName !== null) {
            // Création de l'utilisateur
            require 'assets/classes/User.php';
            $user = new User($usersCollection);
            $userData = [
                'firstName' => $firstName,
                'lastName' => $lastName
            ];
            $user->createUser($userData);
        }

        // Validation des données d'entrée
        $productName = isset($_POST['productName']) ? htmlspecialchars($_POST['productName']) : null;

        if ($productName !== null) {
            // Création du produit
            require 'assets/classes/Product.php';
            $product = new Product($productsCollection);
            $productData = [
                'productName' => $productName
            ];
            $product->createProduct($productData);
        }

        // Validation des données d'entrée
        $categoryName = isset($_POST['categoryName']) ? htmlspecialchars($_POST['categoryName']) : null;

        if ($categoryName !== null) {
            // Création de la categorie
            require 'assets/classes/Category.php';
            $category = new Category($categoriesCollection);
            $categoryData = [
                'categoryName' => $categoryName
            ];
            $category->createCategory($categoryData);
        }

        // Redirection vers la page principale
        header("Location: index.php");
    }
    catch (MongoDBException $e) {
        // Gérer les exceptions MongoDB
        echo "Erreur lors de l'accès à MongoDB : " . $e->getMessage();
        exit;
    }
}
?>

